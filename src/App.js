import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import ShowResult from "./ShowResult";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = { value1: 10, result: 0 };

    this.addToResult = this.addToResult.bind(this);
  }

  addToResult(value) {
    let result = value + this.state.result;

    this.setState({ result: result });
  }

  componentWillMount() {
    console.log("Mount Started");
  }
  componentDidMount() {
    console.log("Mounte Completed");
  }

  render() {
    console.log(this.state);
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to {this.props.sessionName}</h1>
        </header>
        <ShowResult result={this.state.result} addToResult={this.addToResult} />
      </div>
    );
  }
}

export default App;
