import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";

const Name = "React session";
ReactDOM.render(
  <div>
    <App sessionName={Name} value="5" />
  </div>,
  document.getElementById("root")
);
registerServiceWorker();
