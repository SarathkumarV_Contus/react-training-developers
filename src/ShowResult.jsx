import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class ShowResult extends Component {

  render() {  
    return (
        <p className="App-intro">
          Grand Total Value = {this.props.result} <br />
          <input type="button" value="Add" onClick={ ()=>this.props.addToResult(45)   } />
        </p>
    );
  }
}

export default ShowResult;
